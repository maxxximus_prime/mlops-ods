# 1.Скачать необходимые библиотеки для линтинга и форматинга(ruff)
```
pip install ruff
# Линтинг
ruff check                          # Lint all files in the current directory (and any subdirectories).
ruff check path/to/code/            # Lint all files in `/path/to/code` (and any subdirectories).
ruff check path/to/code/*.py        # Lint all `.py` files in `/path/to/code`.
ruff check path/to/code/to/file.py  # Lint `file.py`.
ruff check @arguments.txt           # Lint using an input file, treating its contents as newline-delimited command-line arguments.

```
# Форматирование

```
ruff format                          # Format all files in the current directory (and any subdirectories).
ruff format path/to/code/            # Format all files in `/path/to/code` (and any subdirectories).
ruff format path/to/code/*.py        # Format all `.py` files in `/path/to/code`.
ruff format path/to/code/to/file.py  # Format `file.py`.
ruff format @arguments.txt           # Format using an input file, treating its contents as newline-delimited command-line arguments.

```


# Настройка хуков pre-commit

```
- repo: https://github.com/astral-sh/ruff-pre-commit
  rev: v0.3.5
  hooks:
    - id: ruff
      args: [ --fix ]
    - id: ruff-format

```
